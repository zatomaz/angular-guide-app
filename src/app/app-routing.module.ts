import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {AuthComponent} from './auth/auth.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },
  { path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule' },
  // { path: 'recipes', component: RecipesComponent, canActivate: [AuthGuard], children: [ // This was moved to recipes-routing.module
  //     { path: '', component: RecipeStartComponent},
  //     { path: 'new', component: RecipeEditComponent},
  //     { path: ':id', component: RecipeDetailComponent, resolve: [RecipesResolverService]},
  //     { path: ':id/edit', component: RecipeEditComponent, resolve: [RecipesResolverService]}
  //   ] },
  { path: 'shopping-list', component: ShoppingListComponent },
  { path: 'auth', component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
