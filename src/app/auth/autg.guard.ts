import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {map, take} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
    return this.authService.user.pipe(take(1), map(user => { // take(1) in order to unsubscribe as soon as we red user's value
      const isAuth = !!user;

      if (isAuth) {
        return true; // If authenticated return true - go to desired path
      }
      return this.router.createUrlTree(['/auth']); // If not authenticated return url tree - go to path /auth
    }));
  }
}
