import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

import {Recipe} from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Hamburger',
  //     'Simple hamburger.',
  //     'https://assets.epicurious.com/photos/57c5c6d9cf9e9ad43de2d96e/6:4/w_620%2Ch_413/the-ultimate-hamburger.jpg',
  //     [
  //       new Ingredient('Meat', 1),
  //       new Ingredient('Bread', 1),
  //       new Ingredient('cheese', 2)
  //     ]),
  //   new Recipe('Cheesecake',
  //     'Best cheesecake in south-west Kamnit.',
  //     'https://www.tasteofhome.com/wp-content/uploads/2017/10/Family-Favorite-Cheesecake_exps9570_HB133235C07_19_5bC_RMS-1-696x696.jpg',
  //     [
  //       new Ingredient('Cookies', 10),
  //       new Ingredient('Philadelphia cheese', 20),
  //       new Ingredient('eggs', 5)
  //     ])
  // ];
  private recipes: Recipe[] = [];

  constructor(private shoppingListService: ShoppingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }
}
