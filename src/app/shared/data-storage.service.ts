import  { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Recipe } from '../recipes/recipe.model';
import {exhaustMap, map, take, tap} from 'rxjs/operators';

import {RecipeService} from '../recipes/recipe.service';
import {AuthService} from '../auth/auth.service';

@Injectable({providedIn: 'root'}) // Optional. Could be added in app.module 'providers:' array.
export class DataStorageService {
  constructor(private http: HttpClient, private recipesService: RecipeService, private authService: AuthService) {}

  storeRecipes() {
    const recipes = this.recipesService.getRecipes();
    this.http.put('https://course-recipe-book-e8a67.firebaseio.com/recipes.json', recipes).subscribe(response => {
      console.log(response);
    });
  }

  fetchRecipes() {
    // Pipe take(1) just takes the current value and then automatically unsubscribes.
    return this.http.get<Recipe[]>('https://course-recipe-book-e8a67.firebaseio.com/recipes.json').pipe(map(recipes => {
      return recipes.map(recipe => { // Add empty ingredients array, when ingredients are missing
        return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
      });
    }),
    tap(recipes => {
      this.recipesService.setRecipes(recipes);
    }));
  }
}
